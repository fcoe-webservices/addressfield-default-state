<?php


/**
 * Allows modules to alter the default values for an address field.
 *
 * @param $default_values
 *   The array of default values. The country is populated from the
 *   'default_country' widget setting.
 * @param $context
 *   An array with the following keys:
 *   - field: The field array.
 *   - instance: The instance array.
 *   - address: The current address values. Allows for per-country defaults.
 */
function addressfield_default_state_addressfield_default_values_alter(&$default_values, $context) {
  // If no other default country was provided, set it to United States.
  // Note: you might want to check $context['instance']['required'] and
  // skip setting the default country if the field is optional.
  if (empty($default_values['country'])) {
    $default_values['country'] = 'US';
  }

  // Determine the country for which other defaults should be provided.
  $selected_country = $default_values['country'];
  if (isset($context['address']['country'])) {
    $selected_country = $context['address']['country'];
  }

  // Add defaults for the US.
  if ($selected_country == 'US') {
    $default_values['state'] = 'California';
    $default_values['administrative_area'] = 'CA';
  }
}